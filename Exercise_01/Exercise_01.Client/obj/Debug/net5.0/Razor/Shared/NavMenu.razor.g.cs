#pragma checksum "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Shared\NavMenu.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6df616ffad54ae11a5a3644a2d4117b54f3fd4f0"
// <auto-generated/>
#pragma warning disable 1591
namespace Exercise_01.Client.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Exercise_01.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Exercise_01.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Exercise_01.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using DevExpress.Blazor;

#line default
#line hidden
#nullable disable
    public partial class NavMenu : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "sidebar" + " " + (
#nullable restore
#line 1 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Shared\NavMenu.razor"
                     StateCssClass

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(2, "b-muwc3rvk2t");
            __builder.OpenComponent<DevExpress.Blazor.DxTreeView>(3);
            __builder.AddAttribute(4, "AllowSelectNodes", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 2 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Shared\NavMenu.razor"
                              true

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(5, "CssClass", "app-sidebar");
            __builder.AddAttribute(6, "Nodes", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenComponent<DevExpress.Blazor.DxTreeViewNode>(7);
                __builder2.AddAttribute(8, "NavigateUrl", "Employees");
                __builder2.AddAttribute(9, "Text", "Pracownicy");
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(10, "\r\n        ");
                __builder2.OpenComponent<DevExpress.Blazor.DxTreeViewNode>(11);
                __builder2.AddAttribute(12, "NavigateUrl", "Departments");
                __builder2.AddAttribute(13, "Text", "Działy Firmy");
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 10 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Shared\NavMenu.razor"
       
    [Parameter] public string StateCssClass { get; set; }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
