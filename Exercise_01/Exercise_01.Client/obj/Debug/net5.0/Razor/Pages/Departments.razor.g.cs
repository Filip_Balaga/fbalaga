#pragma checksum "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "055f7fbf001143071ccc504791567ec6f0a1a04c"
// <auto-generated/>
#pragma warning disable 1591
namespace Exercise_01.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Exercise_01.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Exercise_01.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using Exercise_01.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\_Imports.razor"
using DevExpress.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
using System.Collections.ObjectModel;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/Departments")]
    public partial class Departments : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3>Działy Firmy</h3>\r\n");
            __Blazor.Exercise_01.Client.Pages.Departments.TypeInference.CreateDxDataGrid_0(__builder, 1, 2, 
#nullable restore
#line 9 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                        DepartmentService.GetDepartmentsEditableAsync

#line default
#line hidden
#nullable disable
            , 3, 
#nullable restore
#line 10 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                           nameof(Exercise_01.Shared.Department.ID)

#line default
#line hidden
#nullable disable
            , 4, 
#nullable restore
#line 11 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                               OnRowRemovingAsync

#line default
#line hidden
#nullable disable
            , 5, 
#nullable restore
#line 12 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                               OnRowUpdatingAsync

#line default
#line hidden
#nullable disable
            , 6, 
#nullable restore
#line 13 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                OnRowInsertingAsync

#line default
#line hidden
#nullable disable
            , 7, 
#nullable restore
#line 14 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                         OnInitNewRowAsync

#line default
#line hidden
#nullable disable
            , 8, 
#nullable restore
#line 15 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                       CurrentEditMode

#line default
#line hidden
#nullable disable
            , 9, 
#nullable restore
#line 16 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                           true

#line default
#line hidden
#nullable disable
            , 10, "mw-1100", 11, (__builder2) => {
                __builder2.OpenComponent<DevExpress.Blazor.DxDataGridCommandColumn>(12);
                __builder2.AddAttribute(13, "Width", "120px");
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(14, "\r\n        ");
                __builder2.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(15);
                __builder2.AddAttribute(16, "Caption", "Id");
                __builder2.AddAttribute(17, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 20 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                               nameof(Exercise_01.Shared.Department.ID)

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(18, "EditorVisible", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean?>(
#nullable restore
#line 20 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                                                                                        false

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(19, "\r\n        ");
                __builder2.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(20);
                __builder2.AddAttribute(21, "Caption", "Name");
                __builder2.AddAttribute(22, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 21 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                                 nameof(Exercise_01.Shared.Department.Name)

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
            }
            , 23, (dataItem) => (__builder2) => {
                __Blazor.Exercise_01.Client.Pages.Departments.TypeInference.CreateDxDataGrid_1(__builder2, 24, 25, 
#nullable restore
#line 25 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                   DataGridSelectionMode.None

#line default
#line hidden
#nullable disable
                , 26, 
#nullable restore
#line 26 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                EmployeeService.GetEmployessByDepartmentAsync(dataItem)

#line default
#line hidden
#nullable disable
                , 27, 
#nullable restore
#line 27 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                       onEmployeeEditAsync

#line default
#line hidden
#nullable disable
                , 28, 
#nullable restore
#line 28 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                        OnEmployeeAddAsync

#line default
#line hidden
#nullable disable
                , 29, 
#nullable restore
#line 29 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                               CurrentEditMode

#line default
#line hidden
#nullable disable
                , 30, 
#nullable restore
#line 30 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                              5

#line default
#line hidden
#nullable disable
                , 31, (__builder3) => {
                    __builder3.OpenComponent<DevExpress.Blazor.DxDataGridCommandColumn>(32);
                    __builder3.AddAttribute(33, "DeleteButtonVisible", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 31 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                                          false

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(34, "Width", "120px");
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(35, "\r\n            ");
                    __builder3.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(36);
                    __builder3.AddAttribute(37, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 33 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                      nameof(Exercise_01.Shared.Employee.ID)

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(38, "EditorVisible", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean?>(
#nullable restore
#line 33 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                                                                             false

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(39, "\r\n            ");
                    __builder3.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(40);
                    __builder3.AddAttribute(41, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 34 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                      nameof(Exercise_01.Shared.Employee.FirstName)

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(42, "\r\n            ");
                    __builder3.OpenComponent<DevExpress.Blazor.DxDataGridColumn>(43);
                    __builder3.AddAttribute(44, "Field", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 35 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                      nameof(Exercise_01.Shared.Employee.Surname)

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(45, "\r\n            ");
                    __Blazor.Exercise_01.Client.Pages.Departments.TypeInference.CreateDxDataGridComboBoxColumn_2(__builder3, 46, 47, "Department", 48, 
#nullable restore
#line 36 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                                                   nameof(Exercise_01.Shared.Employee.DepartmentID)

#line default
#line hidden
#nullable disable
                    , 49, 
#nullable restore
#line 36 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                                                                                                                 DepartmentService.GetDepartmentsEditableAsync

#line default
#line hidden
#nullable disable
                    , 50, 
#nullable restore
#line 36 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                                                                                                                                                                                 nameof(Exercise_01.Shared.Department.ID)

#line default
#line hidden
#nullable disable
                    , 51, 
#nullable restore
#line 36 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                                                                                                                                                                                                                                           nameof(Exercise_01.Shared.Department.Name)

#line default
#line hidden
#nullable disable
                    , 52, 
#nullable restore
#line 36 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                                                                                                                                                                                                                                                                                                false

#line default
#line hidden
#nullable disable
                    , 53, 
#nullable restore
#line 36 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                                                                                                                                                                                                                                                                                                                                      true

#line default
#line hidden
#nullable disable
                    );
                }
                , 54, (__value) => {
#nullable restore
#line 24 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                          detailsGrid = __value;

#line default
#line hidden
#nullable disable
                }
                );
            }
            , 55, (__value) => {
#nullable restore
#line 8 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
                  grid = __value;

#line default
#line hidden
#nullable disable
            }
            );
        }
        #pragma warning restore 1998
#nullable restore
#line 40 "C:\Projects\Exercise_01\Exercise_01\Exercise_01.Client\Pages\Departments.razor"
       
    DataGridEditMode currentEditMode = DataGridEditMode.PopupEditForm;
    DataGridEditMode CurrentEditMode
    {
        get => currentEditMode;
        set
        {
            if (currentEditMode != value)
            {
                currentEditMode = value;
                CancelEdit();
            }
        }
    }
    private DxDataGrid<Exercise_01.Shared.Employee> detailsGrid;
    async Task OnRowRemovingAsync(Exercise_01.Shared.Department dataItem)
    {
        await DepartmentService.RemoveDepartmentAsync(dataItem);
        StateHasChanged();
    }
    async Task OnRowUpdatingAsync(Exercise_01.Shared.Department dataItem, IDictionary<string, object> newValues)
    {
        await DepartmentService.UpdateDepartmentAsync(dataItem, newValues);
        StateHasChanged();
    }
    async Task OnRowInsertingAsync(IDictionary<string, object> newValues)
    {
        await DepartmentService.InsertDepartmentAsync(newValues);
        StateHasChanged();
    }
    async Task OnInitNewRowAsync(Dictionary<string, object> values)
    {
        values.Add(nameof(Exercise_01.Shared.Department.Name), "IT");
        await Task.CompletedTask;
    }
    void CancelEdit()
    {
        var cancelTask = Task.Run(async () => await grid.CancelRowEdit());
        cancelTask.GetAwaiter().GetResult();
    }
    //Details data grid
    private DxDataGrid<Exercise_01.Shared.Department> grid;
    async Task OnEmployeeAddAsync(IDictionary<string, object> newValues)
    {
        await EmployeeService.InsertEmployeeAsync(newValues);
        StateHasChanged();
    }
    async Task onEmployeeEditAsync(Exercise_01.Shared.Employee dataItem, IDictionary<string, object> newValues)
    {
        await EmployeeService.UpdateEmployeeAsync(dataItem, newValues);
        StateHasChanged();
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Services.DepartmentService DepartmentService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Services.EmployeeService EmployeeService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
namespace __Blazor.Exercise_01.Client.Pages.Departments
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateDxDataGrid_0<T>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Func<global::System.Threading.CancellationToken, global::System.Threading.Tasks.Task<global::System.Collections.Generic.IEnumerable<T>>> __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Func<T, global::System.Threading.Tasks.Task> __arg2, int __seq3, global::System.Func<T, global::System.Collections.Generic.IDictionary<global::System.String, global::System.Object>, global::System.Threading.Tasks.Task> __arg3, int __seq4, global::System.Func<global::System.Collections.Generic.IDictionary<global::System.String, global::System.Object>, global::System.Threading.Tasks.Task> __arg4, int __seq5, global::System.Func<global::System.Collections.Generic.Dictionary<global::System.String, global::System.Object>, global::System.Threading.Tasks.Task> __arg5, int __seq6, global::DevExpress.Blazor.DataGridEditMode __arg6, int __seq7, global::System.Boolean __arg7, int __seq8, global::System.String __arg8, int __seq9, global::Microsoft.AspNetCore.Components.RenderFragment __arg9, int __seq10, global::Microsoft.AspNetCore.Components.RenderFragment<T> __arg10, int __seq11, global::System.Action<global::DevExpress.Blazor.DxDataGrid<T>> __arg11)
        {
        __builder.OpenComponent<global::DevExpress.Blazor.DxDataGrid<T>>(seq);
        __builder.AddAttribute(__seq0, "DataAsync", __arg0);
        __builder.AddAttribute(__seq1, "KeyFieldName", __arg1);
        __builder.AddAttribute(__seq2, "RowRemovingAsync", __arg2);
        __builder.AddAttribute(__seq3, "RowUpdatingAsync", __arg3);
        __builder.AddAttribute(__seq4, "RowInsertingAsync", __arg4);
        __builder.AddAttribute(__seq5, "InitNewRow", __arg5);
        __builder.AddAttribute(__seq6, "EditMode", __arg6);
        __builder.AddAttribute(__seq7, "ShowDetailRow", __arg7);
        __builder.AddAttribute(__seq8, "CssClass", __arg8);
        __builder.AddAttribute(__seq9, "Columns", __arg9);
        __builder.AddAttribute(__seq10, "DetailRowTemplate", __arg10);
        __builder.AddComponentReferenceCapture(__seq11, (__value) => { __arg11((global::DevExpress.Blazor.DxDataGrid<T>)__value); });
        __builder.CloseComponent();
        }
        public static void CreateDxDataGrid_1<T>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::DevExpress.Blazor.DataGridSelectionMode __arg0, int __seq1, global::System.Func<global::System.Threading.CancellationToken, global::System.Threading.Tasks.Task<global::System.Collections.Generic.IEnumerable<T>>> __arg1, int __seq2, global::System.Func<T, global::System.Collections.Generic.IDictionary<global::System.String, global::System.Object>, global::System.Threading.Tasks.Task> __arg2, int __seq3, global::System.Func<global::System.Collections.Generic.IDictionary<global::System.String, global::System.Object>, global::System.Threading.Tasks.Task> __arg3, int __seq4, global::DevExpress.Blazor.DataGridEditMode __arg4, int __seq5, global::System.Int32 __arg5, int __seq6, global::Microsoft.AspNetCore.Components.RenderFragment __arg6, int __seq7, global::System.Action<global::DevExpress.Blazor.DxDataGrid<T>> __arg7)
        {
        __builder.OpenComponent<global::DevExpress.Blazor.DxDataGrid<T>>(seq);
        __builder.AddAttribute(__seq0, "SelectionMode", __arg0);
        __builder.AddAttribute(__seq1, "DataAsync", __arg1);
        __builder.AddAttribute(__seq2, "RowUpdatingAsync", __arg2);
        __builder.AddAttribute(__seq3, "RowInsertingAsync", __arg3);
        __builder.AddAttribute(__seq4, "EditMode", __arg4);
        __builder.AddAttribute(__seq5, "PageSize", __arg5);
        __builder.AddAttribute(__seq6, "ChildContent", __arg6);
        __builder.AddComponentReferenceCapture(__seq7, (__value) => { __arg7((global::DevExpress.Blazor.DxDataGrid<T>)__value); });
        __builder.CloseComponent();
        }
        public static void CreateDxDataGridComboBoxColumn_2<T>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.String __arg0, int __seq1, global::System.String __arg1, int __seq2, global::System.Func<global::System.Threading.CancellationToken, global::System.Threading.Tasks.Task<global::System.Collections.Generic.IEnumerable<T>>> __arg2, int __seq3, global::System.String __arg3, int __seq4, global::System.String __arg4, int __seq5, global::System.Boolean __arg5, int __seq6, global::System.Boolean? __arg6)
        {
        __builder.OpenComponent<global::DevExpress.Blazor.DxDataGridComboBoxColumn<T>>(seq);
        __builder.AddAttribute(__seq0, "Caption", __arg0);
        __builder.AddAttribute(__seq1, "Field", __arg1);
        __builder.AddAttribute(__seq2, "DataAsync", __arg2);
        __builder.AddAttribute(__seq3, "ValueFieldName", __arg3);
        __builder.AddAttribute(__seq4, "TextFieldName", __arg4);
        __builder.AddAttribute(__seq5, "Visible", __arg5);
        __builder.AddAttribute(__seq6, "EditorVisible", __arg6);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
