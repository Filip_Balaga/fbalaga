﻿using Exercise_01.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise_01.Client.Services
{
    public partial class DepartmentService
    {
        public Uri Base { get; set; }
        async public Task<IEnumerable<Department>> GetDepartmentsEditableAsync(CancellationToken ct = default)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                return await client.GetFromJsonAsync<IEnumerable<Department>>(""); ;
            }
        }
        async public Task InsertDepartmentAsync(IDictionary<string, object> newValues)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                Dictionary<string, string> dString = newValues.ToDictionary(k => k.Key, k => k.Value.ToString());
                await client.PostAsJsonAsync("", dString);
            }
        }
        async public Task RemoveDepartmentAsync(Department dataItem)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                var res = await client.DeleteAsync(client.BaseAddress + "/" + dataItem.ID);
            }
        }
        async public Task UpdateDepartmentAsync(Department dataItem, IDictionary<string, object> newValues)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                if (newValues.TryGetValue("Name", out object name))
                {
                    dataItem.Name = name.ToString();
                }
                await client.PutAsJsonAsync(client.BaseAddress + "/" + dataItem.ID, dataItem);
            }
        }
    }
}
