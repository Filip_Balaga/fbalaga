﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Exercise_01.Server.Data;
using Exercise_01.Shared;
using System.Collections.ObjectModel;
using Rksoftware.PropertyCopy;

namespace Exercise_01.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private Exercise_01Context context;
        public EmployeesController(Exercise_01Context _context)
        {
            context = _context;
        }
        [HttpGet("{id}")]
        public Employee Get(int id)
        {
            return context.Employees.Where(o => o.ID == id).FirstOrDefault();
        }
        [HttpGet]
        public ActionResult<List<Employee>> Get()
        {
            return context.Employees.ToList();
        }
        [HttpPost]
        public ActionResult Post(Employee employee)
        {
            try
            {
                if (employee is null)
                {
                    throw new ArgumentNullException(nameof(employee));
                }
                context.Employees.Add(employee);
                context.SaveChanges();
                return StatusCode(201);
            }
            catch
            {
                return StatusCode(400);
            }
        }
        [HttpPut("{id}")]
        public ActionResult Put([FromBody] Employee employee, int id)
        {
            try
            {
                if (employee is null)
                {
                    throw new ArgumentNullException(nameof(employee));
                }
                var exist = context.Employees.Where(o => o.ID == id).FirstOrDefault();
                if (exist != null)
                {
                    PropertyCopier.CopyTo(employee, exist);
                    context.SaveChanges();
                    return StatusCode(201);
                }
                else
                {
                    return StatusCode(404);
                }
            }
            catch
            {
                return StatusCode(400);
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                context.Remove(context.Employees.Where(o => o.ID == id).FirstOrDefault());
                context.SaveChanges();
                return StatusCode(201);
            }
            catch
            {
                return StatusCode(400);
            }
        }
    }
}