﻿using Exercise_01.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise_01.Server.Data
{
    public class Exercise_01Context : DbContext
    {
        public Exercise_01Context(DbContextOptions<Exercise_01Context> options) : base(options)
        {
        }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Department>().ToTable("FB_Department");
            modelBuilder.Entity<Employee>().ToTable("FB_Employee");
        }
    }
}
