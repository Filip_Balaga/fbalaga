﻿using System.ComponentModel.DataAnnotations;

namespace Exercise_01.Shared
{
    public class Employee
    {
        [Key]
        public int ID { get; set; }

        public int DepartmentID { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public Department Department { get; set; }

        /*
        public Employee(int id, int departmentID, string firstName, string surname, Department department)
        {
            ID = id;
            DepartmentID = departmentID;
            FirstName = firstName;
            Surname = surname;
            Department = department;
        }*/
    }
}