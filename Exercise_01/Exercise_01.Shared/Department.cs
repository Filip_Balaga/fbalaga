﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_01.Shared
{
    public class Department
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public List<Employee> Employees { get; set; }

        
        /*public Department(int id, string name)
        {
            ID = id;
            Name = name;
        }*/
    }
}
